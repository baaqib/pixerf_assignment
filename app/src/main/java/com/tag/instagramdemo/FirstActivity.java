package com.tag.instagramdemo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tag.instagramdemo.classes.ApplicationData;
import com.tag.instagramdemo.classes.ConnectionState;
import com.sa90.materialarcmenu.ArcMenu;
import com.tag.instagramdemo.classes.InstagramApp;
import com.tag.instagramdemo.lazyload.ImageLoader;

import java.util.HashMap;

public class FirstActivity extends AppCompatActivity  {


    private InstagramApp mApp;
    private TextView btnConnect,btnImages;
    FloatingActionButton profile,images,developer,logout;
    public ArcMenu fab;



    private HashMap<String, String> userInfoHashmap = new HashMap<String, String>();
    private Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == InstagramApp.WHAT_FINALIZE) {
                userInfoHashmap = mApp.getUserInfo();
            } else if (msg.what == InstagramApp.WHAT_FINALIZE) {
                Toast.makeText(FirstActivity.this, "Check your network.",
                        Toast.LENGTH_SHORT).show();
            }
            return false;
        }
    });

    @Override
    public void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        btnConnect = (TextView) findViewById(R.id.btnConnect);
        btnImages = (TextView) findViewById(R.id.btnImages);
        fab = (ArcMenu) findViewById(R.id.arcMenu);
        logout = (FloatingActionButton) findViewById(R.id.logout);
        developer = (FloatingActionButton) findViewById(R.id.developer);
        images = (FloatingActionButton) findViewById(R.id.images);
        profile = (FloatingActionButton) findViewById(R.id.profile);



        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectionState.getConnectionState(getApplicationContext())) {
                    connectOrDisconnectUser();
                }else {
                    Toast.makeText(getApplicationContext(),"no internet connection",Toast.LENGTH_LONG).show();
                }
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectOrDisconnectUser();
            }
        });

        developer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                        FirstActivity.this);
                alertDialog.setTitle("Developed By");

                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.developer_view, null);
                alertDialog.setView(view);

                TextView tvName = (TextView) view.findViewById(R.id.dname);
                TextView contact = (TextView) view.findViewById(R.id.contact);
                TextView qualification = (TextView) view.findViewById(R.id.qualification);

                tvName.setText("Aaqib Habib");
                qualification.setText("Btech CSE");
                contact.setText("baaqib2@gmail.com");
                alertDialog.create().show();
            }
        });
        images.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ConnectionState.getConnectionState(getApplicationContext())) {
                    startActivity(new Intent(FirstActivity.this, ImagesActivity.class)
                            .putExtra("userInfo", userInfoHashmap));
                }else {
                    Toast.makeText(getApplicationContext(),"no internet connection",Toast.LENGTH_LONG).show();
                }
            }
        });
        btnImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectionState.getConnectionState(getApplicationContext())) {
                    startActivity(new Intent(FirstActivity.this, ImagesActivity.class)
                            .putExtra("userInfo", userInfoHashmap));
                }else {
                    Toast.makeText(getApplicationContext(),"no internet connection",Toast.LENGTH_LONG).show();
                }
            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayInfoDialogView();
            }
        });

        mApp = new InstagramApp(this, ApplicationData.CLIENT_ID,
                ApplicationData.CLIENT_SECRET, ApplicationData.CALLBACK_URL);
        mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {

            @Override
            public void onSuccess() {
                // tvSummary.setText("Connected as " + mApp.getUserName());
                btnConnect.setVisibility(View.INVISIBLE);
                fab.setVisibility(View.VISIBLE);
                btnImages.setVisibility(View.VISIBLE);
                // userInfoHashmap = mApp.
                mApp.fetchUserName(handler);
            }

            @Override
            public void onFail(String error) {
                Toast.makeText(FirstActivity.this, error, Toast.LENGTH_SHORT)
                        .show();
            }
        });


        if (mApp.hasAccessToken()) {
            // tvSummary.setText("Connected as " + mApp.getUserName());
            btnConnect.setVisibility(View.INVISIBLE);
            fab.setVisibility(View.VISIBLE);
            btnImages.setVisibility(View.VISIBLE);
            mApp.fetchUserName(handler);






        } else {
            fab.setVisibility(View.INVISIBLE);
            btnImages.setVisibility(View.INVISIBLE);
        }


    }



    private void connectOrDisconnectUser() {
        if (mApp.hasAccessToken()) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(
                    FirstActivity.this);
            builder.setMessage("Are You Sure to logout?")
                    .setCancelable(false)
                    .setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    mApp.resetAccessToken();
                                    // btnConnect.setVisibility(View.VISIBLE);
                                    fab.setVisibility(View.INVISIBLE);
                                    btnImages.setVisibility(View.INVISIBLE);
                                    btnConnect.setVisibility(View.VISIBLE);
                                    btnConnect.setText("Login");
                                    // tvSummary.setText("Not connected");
                                }
                            })
                    .setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                }
                            });
            final AlertDialog alert = builder.create();
            alert.show();
        } else {
            mApp.authorize();
        }
    }

    private void displayInfoDialogView() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                FirstActivity.this);
        alertDialog.setTitle("Profile");

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.profile_view, null);
        alertDialog.setView(view);
        ImageView ivProfile = (ImageView) view
                .findViewById(R.id.ivProfileImage);
        TextView tvName = (TextView) view.findViewById(R.id.tvUserName);
        TextView tvNoOfFollwers = (TextView) view
                .findViewById(R.id.tvNoOfFollowers);
        TextView tvNoOfFollowing = (TextView) view
                .findViewById(R.id.tvNoOfFollowing);
        new ImageLoader(FirstActivity.this).DisplayImage(
                userInfoHashmap.get(InstagramApp.TAG_PROFILE_PICTURE),
                ivProfile);
        tvName.setText(userInfoHashmap.get(InstagramApp.TAG_USERNAME));
        tvNoOfFollowing.setText(userInfoHashmap.get(InstagramApp.TAG_FOLLOWS));
        tvNoOfFollwers.setText(userInfoHashmap
                .get(InstagramApp.TAG_FOLLOWED_BY));
        alertDialog.create().show();
    }
}